<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function kesehatan()
    {
    	return view('kesehatan');
    }

    public function psikotes()
    {
    	return view('psikotes');
    }

    public function kesamaptaan()
    {
    	return view('kesamaptaan');
    }

    public function wawancara()
    {
    	return view('wawancara');
    }
}
