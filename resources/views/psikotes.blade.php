<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Formulir Hasil Psikotes</title>


        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('bootstrap_3.3.7/css/bootstrap.min.css')}}"  rel="stylesheet">

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Open Sans';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
      <!--
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <marquee>SISTEM-VERIFIKASI-BANK-BTN</marquee>
                </div>
            </div>
        </div>
      -->
        <div class="container">

          <form class="form-horizontal" action="" method="post">
            {{ csrf_field() }}
            <hr>
            <center><h3>FORM PSIKOTES</h3></center>
            <hr>
            <div class="form-group">
              <label for="" class="col-md-offset-1 col-sm-2 control-label">Nomor Urut</label>
              <div class="col-sm-6">
                <p>314770024246</p>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-md-offset-1 col-sm-2 control-label">Nama</label>
              <div class="col-sm-6">
                <p>Nicolas Novian Ruslim</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Lokasi Tes</label>
              <div class="col-sm-2">
                <p>ATKP Surabaya</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Jenis Kelamin</label>
              <div class="col-sm-2">
                <p>Laki-Laki</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Usia</label>
              <div class="col-sm-2">
                <p>18 tahun 6 bulan</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Sekolah Tujuan</label>
              <div class="col-sm-2">
                <p>ATKP Surabaya</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Jurusan</label>
              <div class="col-sm-2">
                <p>D.III MTU</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">IQ</label>
              <div class="col-sm-1">
                <input type="number" name="iq" class="form-control" value="">
              </div>
            </div>

            <h3>1. Kemampuan Dasar</h3><br>
            <br>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Umum</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Verbal</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Hitung</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Dasar Keteknikan</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan ORB</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Non Verbal / Berpikir</label>
              <div class="col-sm-1">
                <input type="number" min="40" max="100" class="form-control" name="berat_badan" value="">
              </div>
            </div>

            <h3>2. Sikap Kerja</h3>
            <br>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Ketelitian</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kecepatan Kerja</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Ketahanan Kerja</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <h3>3. Kepribadian</h3>
            <br>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Penyesuaian Diri</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Pengambilan Keputusan</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kestabilan Emosi</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div> 

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kerjasama</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Kemampuan Melayani</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Motivasi Berprestasi</label>
              <div class="col-sm-1">
                <input type="number" min="0" max="5" class="form-control" name="tinggi" value="">
              </div> 
            </div> 

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Catatan Khusus</label>
              <div class="col-sm-6">
                <textarea class="form-control" name="keterangan" rows="3"></textarea>
              </div>
            </div>
            <div class="form-group ">
              <div class="col-md-offset-4 col-sm-4">
                <input type="submit" class="col-md-offset-1 btn btn-primary"  value="SIMPAN DATA">
              </div>
            </div>
          </form>
        </div>

    </body>
</html>
